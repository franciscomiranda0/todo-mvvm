import 'package:flutter/material.dart';
import 'package:todo/model/todo_in_memory_repository.dart';
import 'package:todo/model/todo_repository.dart';
import 'package:todo/model/todo_shared_preferences_repository.dart';
import 'package:todo/view/widgets/todo_form.dart';
import 'package:todo/view/widgets/todo_listing.dart';
import 'package:todo/view_model/todo_view_model.dart';

class TodoView extends StatelessWidget {
  final _todoViewModel = TodoViewModel(
    repository: TodoSharedPreferencesRepository(),
    // repository: TodoInMemoryRepository(),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Todo'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            TodoForm(
              todoViewModel: _todoViewModel,
            ),
            TodoListing(
              todoViewModel: _todoViewModel,
            ),
          ],
        ),
      ),
    );
  }
}
