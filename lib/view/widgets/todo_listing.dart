import 'package:flutter/material.dart';
import 'package:todo/view_model/todo_view_model.dart';

class TodoListing extends StatelessWidget {
  final TodoViewModel todoViewModel;

  const TodoListing({
    Key? key,
    required this.todoViewModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: todoViewModel,
      builder: (_, __) {
        final _todos = todoViewModel.getTodos();
        return ListView.builder(
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: _todos.length,
          itemBuilder: (context, int index) {
            return ListTile(
              subtitle: Text(_todos[index].description!),
              title: Text(_todos[index].name),
              trailing: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  IconButton(
                    onPressed: () =>
                        todoViewModel.setEditingTodo(_todos[index]),
                    icon: Icon(
                      Icons.edit,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  IconButton(
                    onPressed: () =>
                        todoViewModel.handleDelete(_todos[index].id),
                    icon: Icon(
                      Icons.cancel,
                      color: Theme.of(context).errorColor,
                    ),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }
}
