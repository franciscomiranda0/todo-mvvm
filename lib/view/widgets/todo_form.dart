import 'package:flutter/material.dart';
import 'package:todo/view_model/todo_view_model.dart';

class TodoForm extends StatelessWidget {
  final GlobalKey<FormState> _formKey;
  final TodoViewModel todoViewModel;

  TodoForm({
    Key? key,
    required this.todoViewModel,
  })  : _formKey = GlobalKey<FormState>(),
        super(key: key);

  bool get _isFormValid => _formKey.currentState!.validate();

  void _unfocus(BuildContext context) => FocusScope.of(context).unfocus();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 8.0,
      ),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            TextFormField(
              controller: todoViewModel.nameController,
              validator: todoViewModel.validateInput,
              decoration: InputDecoration(
                hintText: 'digite o nome',
                labelText: 'Nome',
              ),
            ),
            TextFormField(
              controller: todoViewModel.descriptionController,
              decoration: InputDecoration(
                hintText: 'digite a descrição',
                labelText: 'Descrição (opcional)',
              ),
            ),
            ElevatedButton(
              onPressed: () {
                if (_isFormValid) {
                  todoViewModel.handleSave();
                  _unfocus(context);
                }
              },
              child: Text('Salvar'),
            )
          ],
        ),
      ),
    );
  }
}
