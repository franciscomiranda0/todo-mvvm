import 'package:todo/model/todo.dart';
import 'package:todo/model/todo_repository.dart';

class TodoInMemoryRepository extends ID implements TodoCrudRepository {
  final _todos = <Todo>[];

  @override
  void create({required String name, String? description}) {
    incrementID();
    _todos.add(Todo(
      id: id,
      name: name,
      description: description,
    ));
  }

  @override
  void delete(int id) {
    _todos.removeWhere((element) => element.id == id);
  }

  @override
  List<Todo> read() {
    return _todos;
  }

  @override
  void update({required int id, String? name, String? description}) {
    final _index = _todos.indexWhere((element) => element.id == id);
    final _todo = _todos.firstWhere((element) => element.id == id);
    _todos.removeAt(_index);
    _todos.insert(
        _index,
        _todo.copyWith(
          name: name,
          description: description,
        ));
  }
}
