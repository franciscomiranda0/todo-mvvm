import 'dart:convert';

class Todo {
  final int id;
  final String name;
  final String? description;

  Todo({
    required this.id,
    required this.name,
    this.description,
  });

  factory Todo.fromJson(Map<String, dynamic> json) {
    return Todo(
      id: json['id'],
      name: json['name'],
      description: json['description'],
    );
  }

  String toJson() {
    final _map = {
      'id': id,
      'name': name,
      'description': description,
    };
    return jsonEncode(_map);
  }

  Todo copyWith({
    String? name,
    String? description,
  }) {
    return Todo(
      id: id,
      name: name ?? this.name,
      description: description ?? this.description,
    );
  }
}
