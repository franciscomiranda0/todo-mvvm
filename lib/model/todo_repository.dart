import 'package:todo/model/todo.dart';

abstract class TodoCrudRepository
    implements CreateTodo, ReadTodo, UpdateTodo, DeleteTodo {}

abstract class CreateTodo {
  void create({
    required String name,
    String? description,
  });
}

abstract class ReadTodo {
  List<Todo> read();
}

abstract class UpdateTodo {
  void update({
    required int id,
    String? name,
    String? description,
  });
}

abstract class DeleteTodo {
  void delete(int id);
}

abstract class ID {
  int _id = 0;

  int get id => _id;

  void incrementID() => _id++;
}
