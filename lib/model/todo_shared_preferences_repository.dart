import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:todo/model/todo.dart';
import 'package:todo/model/todo_repository.dart';

class TodoSharedPreferencesRepository extends ID implements TodoCrudRepository {
  late final SharedPreferences _sharedPreferences;
  final String _dbKey = 'todoDB';

  TodoSharedPreferencesRepository() {
    _initializeSharedPreferences();
  }

  void _initializeSharedPreferences() async {
    try {
      _sharedPreferences = await SharedPreferences.getInstance();
    } catch (e) {
      SharedPreferences.setMockInitialValues({});
      _sharedPreferences = await SharedPreferences.getInstance();
    }
  }

  @override
  void create({
    required String name,
    String? description,
  }) async {
    incrementID();
    final _todos = read();
    _todos.add(Todo(
      id: id,
      name: name,
      description: description,
    ));
    _save(_todos);
  }

  @override
  List<Todo> read() {
    return _toModels(_strings);
  }

  List<Todo> _toModels(List<String>? strings) {
    final _todos = <Todo>[];
    if (strings != null) {
      for (final item in strings) {
        _todos.add(
          Todo.fromJson(jsonDecode(item)),
        );
      }
    }
    return _todos;
  }

  List<String>? get _strings {
    return _sharedPreferences.getStringList(_dbKey);
  }

  void _save(List<Todo> todos) async {
    _sharedPreferences.setStringList(_dbKey, _toStrings(todos));
  }

  List<String> _toStrings(List<Todo> todos) {
    final _strings = <String>[];
    for (var item in todos) _strings.add(item.toJson());
    return _strings;
  }

  @override
  void update({
    required int id,
    String? name,
    String? description,
  }) {
    final _todos = read();
    final _index = _todos.indexWhere((e) => e.id == id);
    final _todo = _todos.firstWhere((e) => e.id == id);
    _todos.removeAt(_index);
    _todos.insert(
        _index,
        _todo.copyWith(
          name: name,
          description: description,
        ));
    _save(_todos);
  }

  @override
  void delete(int id) async {
    final _todos = read();
    _todos.removeWhere((e) => e.id == id);
    _save(_todos);
  }
}
