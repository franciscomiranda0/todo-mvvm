import 'package:flutter/material.dart';
import 'package:todo/model/todo.dart';
import 'package:todo/model/todo_repository.dart';

class TodoViewModel extends ChangeNotifier {
  final TextEditingController descriptionController;
  final TextEditingController nameController;
  final TodoCrudRepository repository;
  int? editingTodoId;

  TodoViewModel({
    required this.repository,
  })  : nameController = TextEditingController(),
        descriptionController = TextEditingController();

  void handleSave() {
    _hasEditingTodo
        ? repository.update(
            id: editingTodoId!,
            name: nameController.text,
            description: descriptionController.text,
          )
        : repository.create(
            name: nameController.text,
            description: descriptionController.text,
          );
    _clearControllers();
    notifyListeners();
  }

  bool get _hasEditingTodo => editingTodoId != null;

  void _clearControllers() {
    editingTodoId = null;
    nameController.clear();
    descriptionController.clear();
  }

  void handleDelete(int id) {
    repository.delete(id);
    notifyListeners();
  }

  List<Todo> getTodos() => repository.read();

  void setEditingTodo(Todo todo) {
    editingTodoId = todo.id;
    nameController.text = todo.name;
    descriptionController.text = todo.description ?? '';
  }

  String? validateInput(String? input) {
    if (_hasBelowMinimumCharCount(input)) {
      return 'Digite no mínimo 3 caracteres';
    }
  }

  bool _hasBelowMinimumCharCount(String? input) =>
      input != null && input.length < 3;
}
