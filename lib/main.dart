import 'package:flutter/material.dart';
import 'package:todo/view/todo_view.dart';

void main() {
  runApp(
    MaterialApp(
      home: TodoView(),
    ),
  );
}
